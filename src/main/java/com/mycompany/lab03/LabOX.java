/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab03;

/**
 *
 * @author Admin
 */
class LabOX {

    static boolean checkWin(char[][] GameBoard, char currentPlayer) {
        return true;
    }

    static boolean checkDraw(char[][] GameBoard, char currentPlayer) {
        if(isDraw(GameBoard)){
            return true;
        }
        return false;
    }

    private static boolean isDraw(char[][] GameBoard) {
       for(int i = 0;i< 3;i++){
             for(int j = 0;j<3 ;j++){
                 if (GameBoard[i][j] != 'X' && GameBoard[i][j] != 'O'){
                     return true;
                 }
             }
         }
         return false;
    }
   
}
